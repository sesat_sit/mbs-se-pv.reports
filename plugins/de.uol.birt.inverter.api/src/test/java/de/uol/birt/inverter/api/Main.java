package de.uol.birt.inverter.api;

import org.eclipse.birt.report.engine.api.IRunAndRenderTask;


public class Main {

	private static final String EXAMPLE_NAME = "output/example.pdf";

	public static void main(String[] args) {
		String reportPath = "reports/inverter_monthly_report.rptdesign";
		if (args.length == 1) {
			reportPath = args[0];
		}
		// init birt api
		final Framework framework = new Framework();

		// open and load report
		final IRunAndRenderTask task = framework.openReport(reportPath);

		// set up connection specific parameters
		framework.applyParameter(task, Parameter.JDBC_DRIVER, "com.mysql.jdbc.Driver");
		framework.applyParameter(task, Parameter.URL, "jdbc:mysql://localhost:5029/solarlog");
		framework.applyParameter(task, Parameter.USER, "jh");
		framework.applyParameter(task, Parameter.PASSWORD, "adhDA#");

		// set up report specific parameters
		framework.applyParameter(task, Parameter.PLANT, "a_volk");
		framework.applyParameter(task, Parameter.YEAR, 2011);
		framework.applyParameter(task, Parameter.MONTH, 8);

		// render report
		framework.renderReport(task, OutputProfile.PDF, EXAMPLE_NAME);
	}
}
