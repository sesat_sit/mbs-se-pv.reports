package de.uol.birt.inverter.statistics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import de.uol.birt.inverter.connection.ConnectionManager;

public class QuantilCalculator implements Callable<Float> {

	private final Timestamp startDate;

	private final Timestamp endDate;

	private final float quantil;

	private final ComponentStatistics statistics;

	public QuantilCalculator(final ComponentStatistics statistics,
			final Timestamp startDate, final Timestamp endDate,
			final float quantil) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.quantil = quantil;
		this.statistics = statistics;
	}

	public Float call() throws Exception {
		return calcQuantil(statistics, quantil);
	}

	public float getQuantil() {
		return quantil;
	}

	private float calcQuantil(
			final ComponentStatistics componentStatistics, final float p)
			throws SQLException {
		final float np = componentStatistics.getSize() * p;
		float quartil = -1;
		int numerator = 0;
		if (isInteger(np)) {
			numerator = 2;
		} else {
			numerator = 1;
		}

		final String sqlQuery = " SELECT SUM(tmp.value) / ? AS quantil FROM (SELECT value FROM ts2 WHERE time BETWEEN ? AND ? AND belongs=? ORDER BY value LIMIT ?, ?) AS tmp";
		final PreparedStatement preparedStatement = ConnectionManager
				.instance().prepare(sqlQuery);
		preparedStatement.setInt(1, numerator);
		preparedStatement.setTimestamp(2, startDate);
		preparedStatement.setTimestamp(3, endDate);
		preparedStatement.setLong(4, componentStatistics.getBelongs());
		preparedStatement.setInt(5, (int) np);
		preparedStatement.setInt(6, numerator);

		final ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			quartil = resultSet.getFloat(1);
		}
		resultSet.close();

		return quartil;
	}

	private boolean isInteger(final float p) {
		boolean isInteger = true;
		final String n = String.valueOf(p);
		final int index = n.indexOf(".");
		if (index >= 0) {
			final String sn = n.substring(index + 1);
			final int suffix = Integer.parseInt(sn);
			isInteger = suffix == 0;
		}
		return isInteger;
	}
}
