package de.uol.birt.inverter.statistics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.uol.birt.inverter.connection.ConnectionManager;


public class ComponentStatisticsHandler {

	private static final Log LOG = LogFactory
			.getLog(ComponentStatisticsHandler.class);

	private static List<ComponentStatistics> CACHE;

	private static Timestamp startDate;

	private static Timestamp endDate;

	public static void setUpConnection(final String driver, final String url,
			final String user, final String password) {
		ConnectionManager.open(driver, url, user, password);
	}

	public static List<ComponentStatistics> load(final String plantName,
			final int year, final int month)
			throws SQLException {
		startDate = getTimestamp(year, month);
		endDate = getTimestamp(year, month + 1);
		if (startDate == null || endDate == null) {
			return new ArrayList<ComponentStatistics>();
		}
		List<ComponentStatistics> componentStatisticsList = CACHE;
		if (CACHE == null) {
			componentStatisticsList = loadBasics(plantName);
			CACHE = componentStatisticsList;
		}

		return componentStatisticsList;
	}

	private static List<ComponentStatistics> loadBasics(final String plantName)
			throws SQLException {
		final List<ComponentStatistics> componentStatisticsList = new ArrayList<ComponentStatistics>();

		final String sqlQuery = "SELECT names.name, MIN(tsValues.value), MAX(tsValues.value), COUNT(tsValues.value), names.belongs FROM (SELECT v.belongs, v.value FROM ts2 AS v WHERE v.belongs IN (SELECT belongs FROM tsnames WHERE name LIKE ? ) AND v.time BETWEEN ? AND ?) AS tsValues INNER JOIN (SELECT t.name, t.belongs FROM tsnames AS t WHERE t.name LIKE ?) AS names ON names.belongs = tsValues.belongs GROUP BY names.name ORDER BY names.name";
		final PreparedStatement preparedStatement = ConnectionManager
				.instance().prepare(sqlQuery);
		preparedStatement.setString(1, plantName.concat(".%"));
		preparedStatement.setTimestamp(2, startDate);
		preparedStatement.setTimestamp(3, endDate);
		preparedStatement.setString(4, plantName.concat(".%"));
		
		final ResultSet resultSet = preparedStatement.executeQuery();
		final ExecutorService executor = Executors.newCachedThreadPool();
		final List<QuantilCalculatorTask> taskList = new ArrayList<QuantilCalculatorTask>();

		while (resultSet.next()) {
			final ComponentStatistics componentStatistics = new ComponentStatistics();

			componentStatistics.setName(censorComponentName(resultSet
					.getString(1))); // name

			componentStatistics.setMin(resultSet.getInt(2)); // min
			componentStatistics.setMax(resultSet.getInt(3)); // max
			componentStatistics.setSize(resultSet.getInt(4)); // size
			componentStatistics.setBelongs(resultSet.getLong(5)); // belongs

			final QuantilCalculatorTask lowerQuantilCalculator = new QuantilCalculatorTask(
					componentStatistics, startDate, endDate, 0.25f);
			executor.execute(lowerQuantilCalculator);
			taskList.add(lowerQuantilCalculator);
			final QuantilCalculatorTask medianCalculator = new QuantilCalculatorTask(
					componentStatistics, startDate, endDate, 0.5f);
			executor.execute(medianCalculator);
			taskList.add(medianCalculator);
			final QuantilCalculatorTask upperCalculator = new QuantilCalculatorTask(
					componentStatistics, startDate, endDate, 0.75f);
			executor.execute(upperCalculator);
			taskList.add(upperCalculator);
			
			componentStatisticsList.add(componentStatistics);
		}
		resultSet.close();

		for (final QuantilCalculatorTask singleTask : taskList) {
			final ComponentStatistics componentStatistics = singleTask
					.getStatistics();
			Float calculatedQuantil = null;
			try {
				calculatedQuantil = singleTask.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			if (singleTask.getQuantilCalculator().getQuantil() == 0.25f) {
				componentStatistics.setLowerQuartil(calculatedQuantil);
			} else if (singleTask.getQuantilCalculator().getQuantil() == 0.5f) {
				componentStatistics.setMedian(calculatedQuantil);
			} else {
				componentStatistics.setUpperQuartil(calculatedQuantil);
			}
		}
		executor.shutdown();

		return componentStatisticsList;
	}

	private static String censorComponentName(final String name) {
		final int index = name.indexOf(".");
		return name.substring(index);
	}

	private static Timestamp getTimestamp(final int year, final int month) {
		String monthDesc = month < 10 ? "0" + month : String.valueOf(month);
		final String time = year + "-" + monthDesc + "-01 00:00:00.0";
		Timestamp timestamp = null;
		try {
			timestamp = Timestamp.valueOf(time);
		} catch (Exception e) {
			LOG.error("Wrong timestamp: " + time + ". Null will be returned.");
		}

		return timestamp;
	}
}