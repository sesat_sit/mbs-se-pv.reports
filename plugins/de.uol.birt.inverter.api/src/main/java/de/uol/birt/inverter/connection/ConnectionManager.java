package de.uol.birt.inverter.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class ConnectionManager {

	private static ConnectionManager connectionManager;

	private Connection connection;

	private ConnectionManager() {
	}

	public static ConnectionManager instance() {
		return connectionManager;
	}

	public static ConnectionManager open(final String driver, final String url,
			final String user, final String password) {
		if (connectionManager == null) {
			connectionManager = new ConnectionManager();
			connectionManager.openConnection(driver, url, user, password);
		}
		return instance();
	}

	private void openConnection(final String driver, final String url,
			final String user, final String password) {
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet executeQuery(final String sqlQuery) throws SQLException {
		final Statement statement = connection.createStatement();
		final ResultSet resultSet = statement.executeQuery(sqlQuery);
		return resultSet;
	}

	public PreparedStatement prepare(final String sqlQuery) throws SQLException {
		return connection.prepareStatement(sqlQuery);
	}
}
