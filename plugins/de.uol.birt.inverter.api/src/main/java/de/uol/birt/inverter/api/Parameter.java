package de.uol.birt.inverter.api;

public enum Parameter {

	JDBC_DRIVER("jdbc_driver_name"), URL("database_url"), USER("user_name"), PASSWORD(
			"password"), PLANT("plant_name"), YEAR(
			"year"), MONTH("month");

	private final String reportParameter;

	private Parameter(final String reportParameter) {
		this.reportParameter = reportParameter;
	}

	String internalGetReportParameter() {
		return reportParameter;
	}
}
