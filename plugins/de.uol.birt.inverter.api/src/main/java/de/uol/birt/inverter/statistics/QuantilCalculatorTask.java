package de.uol.birt.inverter.statistics;

import java.sql.Timestamp;
import java.util.concurrent.FutureTask;

public class QuantilCalculatorTask extends FutureTask<Float> {

	private final QuantilCalculator quantilCalculator;

	private ComponentStatistics statistics;

	public QuantilCalculatorTask(final ComponentStatistics statistics,
			final Timestamp startDate, final Timestamp endDate,
			final float quantil) {
		this(new QuantilCalculator(statistics, startDate, endDate, quantil));
		this.statistics = statistics;
	}

	private QuantilCalculatorTask(final QuantilCalculator quantilCalculator) {
		super(quantilCalculator);
		this.quantilCalculator = quantilCalculator;
	}

	public QuantilCalculator getQuantilCalculator() {
		return quantilCalculator;
	}

	public ComponentStatistics getStatistics() {
		return statistics;
	}
}
