package de.uol.birt.inverter.api;

import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;

public final class RenderOptionFactory {


	private RenderOptionFactory() {
		// factory
	}

	public static RenderOption getRenderOption(
			final OutputProfile outputProfile, final String fileName) {
		RenderOption renderOption = null;
		switch (outputProfile) {
		case HTML:
			renderOption = new HTMLRenderOption();
			renderOption.setOutputFileName(fileName);
			renderOption.setOutputFormat("html");
			// Setting this to true removes html and body tags
			((HTMLRenderOption) renderOption).setEmbeddable(false);
			break;
		case PDF:
			renderOption = new PDFRenderOption();
			renderOption.setOutputFileName(fileName);
			renderOption.setOutputFormat("pdf");
			break;
		default:
			renderOption = getRenderOption(OutputProfile.PDF, fileName);
			break;
		}

		return renderOption;
	}
}
