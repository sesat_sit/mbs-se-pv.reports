package de.uol.birt.inverter.api;

import java.io.InputStream;
import java.util.logging.Level;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;

public class Framework {

	private static final Log LOG = LogFactory.getLog(Framework.class);

	private IReportEngine engine;

	public Framework() {
		initFramework();
	}

	private void initFramework() {
		final EngineConfig config = new EngineConfig();
		config.setEngineHome("E:/eclipse/birt-runtime-3_7_1/ReportEngine");
		boolean startupSuccess = true;
		try {
			Platform.startup(config);
		} catch (BirtException e) {
			LOG.error("Could not start Framework", e);
			startupSuccess = false;
		}

		if (startupSuccess) {
			IReportEngineFactory factory = (IReportEngineFactory) Platform
					.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			if (factory != null) {
				engine = factory.createReportEngine(config);
				engine.changeLogLevel(Level.WARNING);
				LOG.info("Framework created.");
			} else {
				LOG.info("Could not create Framework.");
			}
		}
	}

	public IRunAndRenderTask openReport(final String path) {
		assert engine != null;

		IReportRunnable design = null;
		try {
			design = engine.openReportDesign(path);
			LOG.info("Report " + path + " read successfully.");
		} catch (EngineException e) {
			LOG.error("Report could not be read.", e);
		}
		return engine.createRunAndRenderTask(design);
	}
	public IRunAndRenderTask openReport(final InputStream reportXmlStream) {
		assert engine != null;
		
		IReportRunnable design = null;
		try {
			design = engine.openReportDesign(reportXmlStream);
			LOG.info("Report read from stream successfully.");
		} catch (EngineException e) {
			LOG.error("Report could not be read.", e);
		}
		return engine.createRunAndRenderTask(design);
	}

	public void applyParameter(final IRunAndRenderTask task,
			final Parameter parameter, final Object parameterValue) {
		task.setParameterValue(parameter.internalGetReportParameter(),
				parameterValue);
	}

	@SuppressWarnings("unchecked")
	public void renderReport(final IRunAndRenderTask task,
			final OutputProfile outputProfile, final String fileName) {
		assert engine != null;

		final boolean isValide = task.validateParameters();

		if (isValide) {
			// Set parent classloader for engine
			task.getAppContext().put(
					EngineConstants.APPCONTEXT_CLASSLOADER_KEY,
					Framework.class.getClassLoader());

			task.setRenderOption(RenderOptionFactory.getRenderOption(
					outputProfile, fileName));

			try {
				task.run();
				LOG.info("Report successfully created (" + fileName + ", "
						+ outputProfile.toString() + ").");
			} catch (Exception e) {
				LOG.error("Could not create report. A Exception occurred.", e);
			} finally {
				task.close();
			}
		} else {
			LOG.error("Could not create report because some parameters are not valid.");
		}
	}
}
