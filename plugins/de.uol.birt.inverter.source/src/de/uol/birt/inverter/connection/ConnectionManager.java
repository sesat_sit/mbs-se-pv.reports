package de.uol.birt.inverter.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public final class ConnectionManager {

	public static final ConnectionManager INSTANCE = new ConnectionManager();

	private Connection connection;

	private ConnectionManager() {
		openConnection();
	}

	private void openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:5029/solarlog", "jh", "adhDA#");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet executeQuery(final String sqlQuery) throws SQLException {
		final Statement statement = connection.createStatement();
		final ResultSet resultSet = statement.executeQuery(sqlQuery);
		return resultSet;
	}
}
