package de.uol.birt.inverter.statistics;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.uol.birt.inverter.connection.ConnectionManager;


public class ComponentStatisticsHandler {

	private static String startDate;

	private static String endDate;

	public static List<ComponentStatistics> load(final String plantName,
			final int year, final int month)
			throws SQLException {
		ComponentStatisticsHandler.startDate = year + "-" + month + "-01";
		ComponentStatisticsHandler.endDate = year + "-" + (month + 1) + "-01";

		final List<ComponentStatistics> componentStatisticsList = loadBasics(plantName);

		return componentStatisticsList;
	}

	private static List<ComponentStatistics> loadBasics(final String plantName)
			throws SQLException {
		final List<ComponentStatistics> componentStatisticsList = new ArrayList<ComponentStatistics>();
		
		final String sqlQuery = "SELECT n.name AS module, MIN(v.value) AS min, MAX(v.value) AS max, COUNT(v.value) AS size FROM tsnames AS n, ts2 AS v WHERE n.name LIKE '"
				+ plantName
				+ ".wr.%' AND n.belongs = v.belongs  AND v.time BETWEEN '"
				+ startDate
				+ "' AND '"
				+ endDate
				+ "' GROUP BY n.name ORDER BY n.name";
		final ResultSet resultSet = ConnectionManager.INSTANCE
				.executeQuery(sqlQuery);

		while (resultSet.next()) {
			final ComponentStatistics componentStatistics = new ComponentStatistics();

			componentStatistics.setName(resultSet.getString(1)); // name
			componentStatistics.setMin(resultSet.getInt(2)); // min
			componentStatistics.setMax(resultSet.getInt(3)); // max
			componentStatistics.setSize(resultSet.getInt(4)); // size

			componentStatistics.setLowerQuartil(calcQuantil(
					componentStatistics, 0.25f));
			componentStatistics.setMedian(calcQuantil(componentStatistics,
					0.50f));
			componentStatistics.setUpperQuartil(calcQuantil(
					componentStatistics, 0.75f));

			componentStatisticsList.add(componentStatistics);
		}
		resultSet.close();

		return componentStatisticsList;
	}

	private static String fillQuery(final int size, final int numerator, final String componentName) {
		StringBuffer sb = new StringBuffer();
		sb = sb.append("SELECT SUM(tmp.value) / " + numerator + " AS quantil ");
		sb = sb.append("FROM (SELECT v.value AS value ");
		sb = sb.append("FROM tsnames AS n, ts2 AS v ");
		sb = sb.append("WHERE n.name = '"
				+ componentName
				+ "' AND n.belongs = v.belongs  AND v.time BETWEEN '"
				+ startDate + "' AND '" + endDate + "' ");
		sb = sb.append("ORDER BY v.value ");
		sb = sb.append("LIMIT " + size + "," + numerator + ") AS tmp");

		return sb.toString();
	}

	private static float calcQuantil(
			final ComponentStatistics componentStatistics, final float p)
			throws SQLException {
		final float np = componentStatistics.getSize() * p;
		float quartil = -1;
		int numerator = 0;
		if (isInteger(np)) {
			numerator = 2;
		} else {
			numerator = 1;
		}

		final String sqlQuery = fillQuery((int) np, numerator,
				componentStatistics.getName());
		final ResultSet resultSet = ConnectionManager.INSTANCE
				.executeQuery(sqlQuery);
		while (resultSet.next()) {
			quartil = resultSet.getFloat(1);
		}
		resultSet.close();

		return quartil;
	}

	private static boolean isInteger(final float p) {
		boolean isInteger = true;
		final String n = String.valueOf(p);
		final int index = n.indexOf(".");
		if (index >= 0) {
			final String sn = n.substring(index + 1);
			final int suffix = Integer.parseInt(sn);
			isInteger = suffix == 0;
		}
		return isInteger;
	}

}