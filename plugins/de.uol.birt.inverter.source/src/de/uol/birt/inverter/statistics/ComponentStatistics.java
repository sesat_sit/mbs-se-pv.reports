package de.uol.birt.inverter.statistics;

public class ComponentStatistics {

	private String name;

	private int min;

	private int max;

	private int size;

	private float lowerQuartil;

	private float upperQuartil;

	private float median;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getMin() {
		return min;
	}

	public void setMin(final int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(final int max) {
		this.max = max;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public float getLowerQuartil() {
		return lowerQuartil;
	}

	public void setLowerQuartil(final float lowerQuartil) {
		this.lowerQuartil = lowerQuartil;
	}

	public float getUpperQuartil() {
		return upperQuartil;
	}

	public void setUpperQuartil(final float upperQuartil) {
		this.upperQuartil = upperQuartil;
	}

	public float getMedian() {
		return median;
	}

	public void setMedian(final float median) {
		this.median = median;
	}
}
